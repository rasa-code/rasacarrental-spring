package lt.rasacode.rasacarrentalspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RasaCarRentalSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(RasaCarRentalSpringApplication.class, args);
    }

}
